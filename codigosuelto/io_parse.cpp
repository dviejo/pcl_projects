
typedef struct
{
    char *inputName;
    char *outputName;
} fileNames_t;




char * findParamName(int, char **, char *);
int parseInput(fileNames_t *, int, char **);





/**
 * Parse standard input to get input and output filenames. 
 * 
 * @return 0: no error; -1: no input filename is present; -2: no output filename is present; -3: no input nor output filenames found
 */
int parseInput(fileNames_t *names, int argc, char **argv)
{
    int ret = 0;
    
    //search for input name
    names->inputName = findParamName(argc, argv, "-i");
    if (names->inputName == nullptr) 
    {
        ret--;
        std::cout<<"Error: input filename not present"<<std::endl;
    }
    
    //search for output name
    names->outputName = findParamName(argc, argv, "-o");
    if (names->outputName == nullptr) 
    {
        ret-=2;
        std::cout<<"Error: output filename not present"<<std::endl;
    }

    if (ret<0) std::cout<<"Usage: $sick2pcd -i inputfilename -o outputfilename"<<std::endl;
    
    return ret;
}


/**
 * Search for a given pattern in an array of char*. If the pattern is found, returns the next element in the array.
 */
char * findParamName(int argc, char **argv, char *searchPattern)
{
    char *ret = nullptr;
    bool found = false;
    
    for(size_t i=0; i<argc-1&&!found; i++)
    {
        if(strcmp(argv[i], searchPattern)==0)
        {
            ret = (char *) malloc(sizeof(argv[i+1]));
            strcpy(ret, argv[i+1]);
            found = true;
        }
    }
    
    return ret;
}
