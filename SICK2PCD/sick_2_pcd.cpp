/*********************************************************************************************
 * 
 * Sick 2 PCD converter
 * 
 * Converts text files containing 3D points used during my PhD experiments into the pcd format
 * 
 * Created by Diego Viejo dviejo@ua.es
 * 07/08/2018
 * 
 * Input file format:
 *      Comentary: # at the begining of the line
 *      Head: first line, 4 integer numbers -> number-of-points-in-file is-color image-values y-inverted
 *              number-of-points-in-file: integer. total number of points.
 *              is-color can take 3 different values:   0 -> no color is provided
 *                                                      1 -> color is stored as a 4 byte integer in RGB format (most significant byte not used)
 *                                                      3 -> color is stored using three char values
 *      Content:
 *          one point per line: x y z [R G B] [imagerow imagecol]
 * 
 * IMPORTANT in some old datasets, header may have no y-inverted value. In the oldest, just the number of points is present.
 * 
 *********************************************************************************************/

#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#define STR_SIZE 256

char *getToken(char *);
int getIntFromString(char *);
double getFloatFromString(char *);
char *nextToken(char *);


int
  main (int argc, char** argv)
{
   
    size_t inputSize, outputSize;
    char * inputFileName;
    char * outputFileName;
    
    FILE *inFile, *outFile;
    
    char *inputString = (char *)malloc(STR_SIZE);
    char *tokenIndex;
    bool headerReady;
    
    int totalPoints, isColor;
    bool imageValues, yInverted;
    
    if(argc < 2 )
    {
        std::cout<<"Error: no input file"<<std::endl<<"Use: sick2pcd inputfilename"<<std::endl;
        return -1;
    }
    
    std::cout<<"Input file name: "<<argv[1]<<std::endl;
    inputFileName = argv[1];
    inputSize = strlen(inputFileName);
    outputSize = inputSize - strlen(strrchr(inputFileName, '.'));
    outputFileName = (char *)malloc(outputSize + 5);
    strncpy(outputFileName, inputFileName, outputSize);
    strcat(outputFileName, ".pcd");
    
    
    //try open input file
    inFile = fopen(inputFileName, "r");
    
    if(inFile == nullptr)
    {
        std::cout<<"Error: can't open "<<inputFileName<<" for reading"<<std::endl;
        return -1;
    }
    
    totalPoints = 0; isColor = 0; imageValues = false; yInverted = false;
    headerReady = false;
    
    while(!headerReady)
    {
        fgets(inputString, STR_SIZE, inFile);
        if(inputString[0] != '#')
        {
            headerReady = true;
            tokenIndex = inputString;
            totalPoints = getIntFromString(tokenIndex);
            tokenIndex = nextToken(tokenIndex);
            if(tokenIndex !=NULL  && strlen(tokenIndex)>0 )
            {
                isColor = getIntFromString(tokenIndex);
                
                tokenIndex = nextToken(tokenIndex);
                if(tokenIndex != NULL  && strlen(tokenIndex)>0 )
                {
                    imageValues = getIntFromString(tokenIndex)!=0;
                    
                    tokenIndex = nextToken(tokenIndex);
                    if(tokenIndex != NULL  && strlen(tokenIndex)>0 )
                    {
                        yInverted = getIntFromString(tokenIndex)!=0;
                    }
                }
            }
        }
    }
    
    std::cout<<"********************* HEADER ***********************"<<std::endl;
    std::cout<<"*                                                  *"<<std::endl;
    std::cout<<"* Points:      "<<totalPoints<<std::endl;
    std::cout<<"* isColor:     "<<isColor<<std::endl;
    std::cout<<"* imageValues: "<<imageValues<<std::endl;
    std::cout<<"* yInverted:   "<<yInverted<<std::endl;
    std::cout<<"*                                                  *"<<std::endl;
    std::cout<<"******************* HEADER END *********************"<<std::endl;
    
    std::cout<<"Output file name: "<<outputFileName<<std::endl;
        
    pcl::PointCloud<pcl::PointXYZ> cloud;

    // Fill in the cloud data
    cloud.width    = totalPoints;
    cloud.height   = 1;
    cloud.is_dense = false;
    cloud.points.resize (cloud.width * cloud.height);

    for (size_t i = 0; i < totalPoints; ++i)
    {
        fgets(inputString, STR_SIZE, inFile);
        if(inputString[0] != '#')
        {
            tokenIndex = inputString;

             cloud.points[i].x = getFloatFromString(tokenIndex);
             tokenIndex = nextToken(tokenIndex);
             cloud.points[i].y = getFloatFromString(tokenIndex);
             tokenIndex = nextToken(tokenIndex);
             cloud.points[i].z = getFloatFromString(tokenIndex);
             tokenIndex = nextToken(tokenIndex);
            
            //TODO read also color and  image values if present.
        }
        else i--;
    }
    pcl::io::savePCDFileASCII ("test_pcd.pcd", cloud);
    std::cerr << "Saved " << cloud.points.size () << " data points to test_pcd.pcd." << std::endl;

//  for (size_t i = 0; i < cloud.points.size (); ++i)
//    std::cerr << "    " << cloud.points[i].x << " " << cloud.points[i].y << " " << cloud.points[i].z << std::endl;
    
  
    fclose(inFile);

  return (0);
}

int getIntFromString(char *input)
{
    return atoi(getToken(input));
}

double getFloatFromString(char *input)
{
    return atof(input);
}

char *getToken(char *input)
{
    char *ret;
    int totalSize = strlen(input);
    int tokenSize = 0;
    for(tokenSize=0; tokenSize<totalSize&&input[tokenSize]!=' ';tokenSize++);
    
    ret = (char *)malloc(sizeof(char)*tokenSize + 1);
    strncpy(ret, input, tokenSize);
    return ret;
}

char *nextToken(char *input)
{
    char *ret;
    ret = index(input, ' ');
    ret++;
    return ret;
}
